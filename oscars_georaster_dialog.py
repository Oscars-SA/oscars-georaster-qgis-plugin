# -*- coding: utf-8 -*-
"""
/***************************************************************************
 ogeorasterDialog
        A QGIS plugin to Access Oracle Georaster using GDAL
                             -------------------
        begin                : 2019-01-02
        git sha              : $Format:%H$
        copyright            : (C) 2019 by Oscars S.A.
        email                : info@oscars-sa.eu
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
"""

import os
import re

from PyQt5 import uic
from PyQt5 import QtWidgets
from PyQt5.QtWidgets import QDialogButtonBox, QMessageBox, QWidget
from PyQt5.QtGui import QPixmap, QDesktopServices
from PyQt5.QtCore import QUrl

# Initialize Qt resources from file resources.py
from .resources import *

from qgis.core import QgsSettings, QgsProviderRegistry
from qgis.utils import iface
from qgis.gui import QgsGui, QgsSourceSelectProvider

from osgeo import gdal

FORM_CLASS, _ = uic.loadUiType(os.path.join(
    os.path.dirname(__file__), 'oscars_georaster_dialog.ui'))


class ogeorasterDialog(QtWidgets.QDialog, FORM_CLASS):

    def __init__(self, parent=None):
        """Constructor."""

        super(ogeorasterDialog, self).__init__(parent)

        #self.gdalId = None
        self.baseId = None

        """Capture Oracle connection dialog (QGIS/src/ui/qgsoraclenewconnectionbase.ui)"""
        #self.dlgDataSourceManager = QgsProviderRegistry.instance().createSelectionWidget('oracle')
        self.dlgDataSourceManager = QgsGui.instance().sourceSelectProviderRegistry().providerByName('oracle').createDataSourceWidget()

        self.setupUi(self)

        """icon_path = ':/plugins/oscars-georaster-qgis-plugin/oscars-sa.png'
        self.oscarsIcon.setPixmap(QPixmap(icon_path))"""
	
        """Widget signals"""
        self.btnConnect.clicked.connect(self.btnConnect_clicked)
        self.btnNew.clicked.connect(self.btnNew_clicked)
        self.btnEdit.clicked.connect(self.btnEdit_clicked)
        self.btnRemove.clicked.connect(self.btnRemove_clicked)
        self.lstResults.itemClicked.connect(self.lstResults_itemClicked)
        self.lstResults.itemDoubleClicked.connect(self.lstResults_itemDoubleClicked)
        self.cmbConnections.currentIndexChanged.connect(self.cmbConnections_currentIndexChanged)
        self.btnSearch.clicked.connect(self.btnSearch_clicked)
        self.buttonBox.helpRequested.connect(self.show_help)

        self.populateConnectionList()

        if gdal.GetDriverByName('GEORASTER') == None:
            QMessageBox.warning(self, u'Plugin will not work', u'GDAL driver "GEORASTER" not installed')

    def populateConnectionList(self):
        """Load existing Oracle connection names from QGIS user settings """

        settings = QgsSettings()
        settings.beginGroup("/Oracle/connections")
        connList = settings.childGroups()
        settings.endGroup()

        selected = settings.value('/Oracle/connections/selected')

        settings = None

        self.cmbConnections.blockSignals(True)
        self.cmbConnections.clear()
        self.cmbConnections.addItems(connList)
        self.cmbConnections.setCurrentIndex(self.cmbConnections.findText(selected))
        self.cmbConnections.blockSignals(False)

        self.btnConnect.setDisabled(self.cmbConnections.count() == 0)
        self.btnEdit.setDisabled(self.cmbConnections.count() == 0)
        self.btnRemove.setDisabled(self.cmbConnections.count() == 0)

        self.buttonBox.button(QDialogButtonBox.Ok).setEnabled(False)
        #self.gdalId = None

    def cmbConnections_currentIndexChanged(self):
        """Updated the selected connection on user's preference"""

        settings = QgsSettings()
        value = settings.value('/Oracle/connections/selected')
        settings = None

    def btnNew_clicked(self):
        """Create new connection"""
        self.dlgDataSourceManager.findChild(QtWidgets.QPushButton,'btnNew').click()
        self.populateConnectionList()

    def btnEdit_clicked(self):
        """Edit current connection"""
        self.dlgDataSourceManager.findChild(QtWidgets.QPushButton,'btnEdit').click()
        self.populateConnectionList()

    def btnRemove_clicked(self):
        """Edit current connection"""
        self.dlgDataSourceManager.findChild(QtWidgets.QPushButton,'btnDelete').click()
        self.populateConnectionList()

    def btnConnect_clicked(self):
        """Save the details of the selected connection"""

        settings = QgsSettings()
        conn = "/Oracle/connections/" + self.cmbConnections.currentText()
        userName   = settings.value( conn + "/username", "", type=str )
        password   = settings.value( conn + "/password", "", type=str )
        hostName   = settings.value( conn + "/host",     "", type=str )
        portNumber = settings.value( conn + "/port",     "", type=str )
        schema     = settings.value( conn + "/schema",   "", type=str )
        database   = settings.value( conn + "/database", "", type=str )

        settings.setValue('/Oracle/connections/selected', conn)
        settings = None

        if hostName == None or len(hostName) == 0:
            self.baseId = "geor:" + userName   + "/" + password   + "@" + database
        else:
            self.baseId = "geor:" + userName   + "/" + password   + "@" \
                                  + hostName   + ":" + portNumber + "/" + database
                                  
        """Query and show results"""
        self.populateResults(self.baseId)

    def lstResults_itemClicked(self, item):
        """Update the selection criteria"""

        self.edtSelected.setText(item.text())

        split = item.text().split(',')
        if len(split) == 3 and split[2].isdigit():
            self.populateResults(item.text())

        self.buttonBox.button(QDialogButtonBox.Ok).setEnabled(True)

        # Layer to load
        #self.gdalId = item.text()

    def lstResults_itemDoubleClicked(self, item):
        """Automatically click btnSearch"""

        self.populateResults(item.text())

    def btnSearch_clicked(self):
        """Use to text on edtSelected to build a query"""

        self.populateResults(self.edtSelected.text())

    def loadMetadata(self, text, skip_next_level = False):
        """Open GDAL dataset"""
        dataset = gdal.Open(text)

        if dataset == None:
            return None, None

        subdatasets = dataset.GetMetadata_List("SUBDATASETS")

        if skip_next_level:
            dataset, subdatasets = self.loadMetadata(self.baseId + ',' + ','.join(subdatasets[0].split(',')[1:]))

        return dataset, subdatasets

    def populateResults(self, text):
        """Query GDAL via georaster indetification string"""

        # Show immediatly tile list of georaster
        if len(text.split(',')) == 2:
            dataset, subdatasets = self.loadMetadata(text, True)
        else:
            dataset, subdatasets = self.loadMetadata(text)

        if dataset == None:
            return

        if subdatasets == None or len(subdatasets) == 0:
            if len(text.split(',')) > 3: # where clause, show as a query result
                rdt = dataset.GetMetadataItem("RDT_TABLE_NAME", "ORACLE")
                rid = dataset.GetMetadataItem("RASTER_ID",      "ORACLE")
                self.lstResults.clear()
                self.lstResults.addItem(self.baseId + ',' + rdt + ',' + rid)
                self.buttonBox.button(QDialogButtonBox.Ok).setEnabled(False)
            else:
                #self.gdalId = text
                dataset = None
                return

        self.lstResults.clear()
        self.buttonBox.button(QDialogButtonBox.Ok).setEnabled(False)

        """Generate resulting list"""
        for subdataset in subdatasets:
            if not re.match(r'SUBDATASET_\d*_NAME', subdataset) == None:
                split = subdataset.split(",")
                if len(split) == 2:
                    self.lstResults.addItem(self.baseId + ',' + split[1])
                if len(split) == 3:
                    self.lstResults.addItem(self.baseId + ',' + split[1] + ',' + split[2])

        dataset = None

    def show_help(self):
        plugin_path = os.path.dirname(__file__)
        help_file = 'file:///%s/help/index.html' % plugin_path
        QDesktopServices.openUrl(QUrl(help_file))
