# README #

The Oscars Georaster Plugin is a QGIS plugin that uses GDAL and GDAL's Georater driver to query and access Oracle Spatial Georaster object from an Oracle server.

### Requirements ###

* QGIS Oracle provider
* GDAL with GDAL's Georaster driver
* QGIS 3 or newer
